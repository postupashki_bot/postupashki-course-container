#include "range.h"

#include <cassert>
#include <vector>

std::vector<int> GetExpectedRange(int start, int end, int step) {
    std::vector<int> res;

    if (step < 0) {
        for (size_t i = start; i > end; i += step) {
            res.push_back(i);
        }

        return res;
    }

    for (size_t i = start; i < end; i += step) {
        res.push_back(i);
    }

    return res;
}

int main() {
    {
        std::vector<int> expected = GetExpectedRange(0, 10, 1);

        std::vector<int> actual;
        for (auto el: Range(10)) {
            actual.push_back(el);
        }

        assert(actual == expected);
    }

    {
        std::vector<int> expected = GetExpectedRange(228, 1488, 1);

        std::vector<int> actual;
        for (auto el: Range(228, 1488)) {
            actual.push_back(el);
        }

        assert(actual == expected);
    }

    {
        std::vector<int> expected = GetExpectedRange(1234, 12345, 5);

        std::vector<int> actual;
        for (auto el: Range(1234, 12345, 5)) {
            actual.push_back(el);
        }

        assert(actual == expected);
    }

    {
        std::vector<int> expected = GetExpectedRange(1000, 0, -1);

        std::vector<int> actual;
        for (auto el: Range(1000, 0, -1)) {
            actual.push_back(el);
        }

        assert(actual == expected);
    }
}