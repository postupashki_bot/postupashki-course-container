template <typename Key, typename Value>
HashTableIterator {
};

template <typename Key, typename Value>
class HashTable {
public:
    bool Insert(const Key& key);

    std::pair<bool, Value> Find(const Key& key) const;

    void Remove(const Key& key);

    Value operator[](const Key& key);

    size_t Size() const;

    HashTableIterator<Key, Value> begin();

    HashTableIterator<Key, Value> end();
};
