# Server

Ваша задача - написать сервер, имитирующий файловую систему, который принимает HTTP запросы по TCP.

На каждый метод вам нужно сделать следующее:

1) GET - выдаёт содержимое файла по URL в теле ответа.
2) POST - создает файл по URL, в теле запроса передается содержимое файла.
3) DELETE - удаляет файл по URL.
4) PUT - изменяет файл по URL, в теле запроса передается содержимое файла.

На каждый метод вы должны отправить соответствующий HTTP ответ.
В случае ошибки нужно вернуть соответствующий код ответа.

## Литература:  
[socket](https://www.opennet.ru/man.shtml?topic=socket&category=2&russian=0)  
[bind](https://man7.org/linux/man-pages/man2/bind.2.html)  
[listen](https://man7.org/linux/man-pages/man2/listen.2.html)  
[http](https://developer.mozilla.org/en-US/docs/Web/HTTP/Messages)  
