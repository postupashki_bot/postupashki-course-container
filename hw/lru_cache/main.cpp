#include "lru_cache.h"

#include <cassert>

int main() {
    {
        LRUCache cache(3);

        cache.Insert("communism", "999");
        cache.Insert("capitalism", "-999");
        cache.Insert("marksizm", "250");

        assert(cache.Find("communism").first);

        cache.Insert("another one", "228");

        assert(cache.Find("another one").first);
        assert(!cache.Find("capitalism").first);

        cache.Insert("a", "1");
        cache.Insert("b", "2");
        cache.Insert("c", "3");

        assert(!cache.Find("another one").first);
        assert(!cache.Find("capitalism").first);
        assert(!cache.Find("marksizm").first);

        assert(cache.Find("a").first);
        assert(cache.Find("a").second == "1");
        assert(cache.Find("b").first);
        assert(cache.Find("b").second == "2");
        assert(cache.Find("c").first);
        assert(cache.Find("c").second == "3");

        cache.Insert("last one", "123");

        assert(!cache.Find("a").first)
    }
}
